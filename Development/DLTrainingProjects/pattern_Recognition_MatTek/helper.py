# -*- coding: utf-8 -*-
"""
Created on Fri May 01 18:41:16 2015

@author: JMS
"""
import os
from os import listdir
from os.path import isfile, join

class charReader:
   tags = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   def __init__(self):
    self.ofile = []
    self.tag = []
    
   def load_digits(self,mypath):
        i = 0;
        for di in listdir(mypath):
            adir = mypath+'\\'+di;           
            onlyfiles = [ f for f in listdir(adir) if isfile(join(adir,f)) ]
            label = self.tags[i];            
            for el in onlyfiles:
                self.ofile.append(adir+'\\'+el);
                self.tag.append(label);
            i= i+1;
        return self.ofile,self.tag;

   def load_digits_by_name(self,mypath):
        i = 0;
        onlyfiles = [ join(mypath,f) for f in listdir(mypath) if isfile(join(mypath,f)) ]
        labels = 0*len(onlyfiles);
        return onlyfiles,labels;


