
import cv2
import numpy as np
from scipy.ndimage.morphology import binary_fill_holes
import matplotlib.pyplot as plt
from skimage import data, io, filters

def splitImage(im_path):
    im = cv2.imread(im_path,0)
    im1 = cv2.resize(im,(256,256))
    w,h = im1.shape
    cxs = 60
    cys = 140
    small_candidates = []
    for k1 in range(-15, 15, 3):
        for k2 in range(-15, 15, 3):
            cxs = 60 + k1
            cys = 140 + k2
            letter = im1[cys-60:cys + 60, cxs-45:cxs + 30]
            h,w = letter.shape
            letterSmall = np.zeros([128, 128], dtype=np.uint8)
            for i in range(h):
                for j in range(w):
                    if (letter[i][j] > 0):
                        if (j+15)<128 and (i+5)<128:
                            letterSmall[i + 5][j + 15] = 255
            small_candidates.append(letterSmall)

    cxb = 150
    cyb = 125
    big_candidates = []
    for k1 in range(-15, 15, 3):
        for k2 in range(-15, 15, 3):
            cxb = 160 + k1
            cyb = 125 + k2
            letter = im1[cyb - 90:cyb + 90, cxb - 70:cxb + 80]
            letterBig = cv2.resize(letter,(128,128))
            big_candidates.append(letterBig)

    # visualize subset of training data
    # fig = plt.figure(figsize=(12, 2))
    # for i in range(0, 12):
    #     ax = fig.add_subplot(1, 12, i + 1)
    #     img = big_candidates[12 + i]
    #     ax.imshow(img)
    # fig.suptitle('Subset of Original Data', fontsize=20)
    # plt.show()
    return small_candidates,big_candidates


def path_to_tensor(img_path):
    img = cv2.imread(img_path,0)
    img = (img<255)*255 # invert
    final = filters.sobel(img)
    final = cv2.resize(final,(224,224))
    final = (final > np.min(final))*255
    # convert 3D tensor to 4D tensor with shape (1, 224, 224, 1) and return 4D tensor
    return np.expand_dims(final, axis=0)


import os
from os import listdir
from os.path import isfile, join

def load_digits(mypath):
    i = 0
    onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
    ofile = []
    tag = []
    for el in onlyfiles:
        if(el[-4:]== '.tif'):
            ofile.append(mypath+'\\'+el)
            tag.append(el[:2])
    return ofile,tag


# impath = "./Fnt1/0B/0B_1.jpg"
# impath = "./Fnt2/0/0_001 - Copy.png"
impath = "./TEST/LETTERS/"
# extra = path_to_tensor(impath)
l1,l2 = load_digits(impath)
sm,big = splitImage(l1[50],l2[50])
th =cv2.imread(impath,0)
m = splitImage(th)