# -*- coding: utf-8 -*-
"""
Created on Mon Jun 01 11:51:39 2015

@author: JMS
"""

def splitImage(im1):  
    contours, hierarchy = cv2.findContours(im1,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    cnt=contours[max_index]
    x,y,w,h = cv2.boundingRect(cnt)            
    letter = im1[y:y+h,x:x+w]
    

    for i in range(h):
        for j in range(w):        
             if(letter[i][j]>0):
                 letter[i][j] = 255 
    
    letter = cv2.resize(letter, (60, 100));
#
#    cv2.imshow('cnt',letter)
#    if cv2.waitKey(0) & 0xff == 27:
#        cv2.destroyAllWindows()
    
    
    size = 256, 256, 1
    m = np.zeros(size, dtype=np.uint8)
    
    for i in range(100):
        for j in range(60):
              if(letter[i][j]>0):
                    m[95+i][35+j] = 255      
     

    im2=  255-im2;   
    contours, hierarchy = cv2.findContours(im2,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    cnt=contours[max_index]
    x,y,w,h = cv2.boundingRect(cnt)            
    letter = im2[y:y+h,x:x+w]
    
    for i in range(h):
        for j in range(w):        
             if(letter[i][j]>0):
                 letter[i][j] = 255 
    letter = cv2.resize(letter, (100, 150));  
          
#    cv2.imshow('cnt',letter)
#    if cv2.waitKey(0) & 0xff == 27:
#        cv2.destroyAllWindows()

    for i in range(150):
        for j in range(100):
              if(letter[i][j]>0):
                    m[50+i][100+j] = 255      
    

    return m
