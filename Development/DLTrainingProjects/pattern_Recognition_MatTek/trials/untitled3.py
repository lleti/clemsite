# -*- coding: utf-8 -*-
"""
Created on Fri May 29 13:13:48 2015

@author: JMS
"""


import numpy as np
import cv2
import os
from helper import charReader
import random

SZ = 256
gxlabels="0123456789ABCDEFGHIJK"
gylabels="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

x = np.arange(len(gxlabels))
y = np.arange(len(gylabels))





def merge_images(im1,im2):
    im1=  255-im1;   
    _, contours, _ = cv2.findContours(im1,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    cnt=contours[max_index]
    x,y,w,h = cv2.boundingRect(cnt)            
    letter = im1[y:y+h,x:x+w]
    

    for i in range(h):
        for j in range(w):        
             if(letter[i][j]>0):
                 letter[i][j] = 255 
    
    letter = cv2.resize(letter, (60, 100));
#
#    cv2.imshow('cnt',letter)
#    if cv2.waitKey(0) & 0xff == 27:
#        cv2.destroyAllWindows()
    
    
    size = 256, 256, 1
    m = np.zeros(size, dtype=np.uint8)
    
    for i in range(100):
        for j in range(60):
              if(letter[i][j]>0):
                    m[95+i][35+j] = 255      
     

    im2=  255-im2;   
    _, contours, _ = cv2.findContours(im2,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    cnt=contours[max_index]
    x,y,w,h = cv2.boundingRect(cnt)            
    letter = im2[y:y+h,x:x+w]
    
    for i in range(h):
        for j in range(w):        
             if(letter[i][j]>0):
                 letter[i][j] = 255 
    letter = cv2.resize(letter, (100, 150));  
          
#    cv2.imshow('cnt',letter)
#    if cv2.waitKey(0) & 0xff == 27:
#        cv2.destroyAllWindows()

    for i in range(150):
        for j in range(100):
              if(letter[i][j]>0):
                    m[50+i][100+j] = 255      
    

    return m


def file_to_img(fn):
    print 'loading "%s" ...' % fn
    digits = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
    ret,thresh = cv2.threshold(digits,127,255,0)
    digits = cv2.resize(thresh, (SZ, SZ)) # normalize to 128 by 128
    return digits

    


def generateTraining():
    myfolder = os.getcwd();

    myfolder2 = myfolder + "\\Fnt2\\"
    myfolder = myfolder + "\\Fnt4\\"

    for i in x:
       for j in y:
           label = gxlabels[i]+gylabels[j]
           nfolder = myfolder+label;
           if not os.path.exists(nfolder):
               os.makedirs(nfolder)




    chread = charReader();
    for i in x:
        minusfolder = myfolder2+gxlabels[i]
        digits_minus, _ = chread.load_digits_by_name(minusfolder)
        digits_minus = random.sample(digits_minus, 5)
        digits_minus = map(file_to_img,digits_minus)
        l=0;
        for el1 in digits_minus:
          l = l+1;
          for j in y:
              plusfolder = myfolder2+gylabels[j]
              digits_plus, _ = chread.load_digits_by_name(plusfolder)
              digits_plus = random.sample(digits_plus, 5)
              digits_plus = map(file_to_img,digits_plus)
              k = 0;
              for el2 in digits_plus:
                 k=k+1;
                 pic=merge_images(el1,el2)
    #             cv2.imshow('merge',pic)
    #             if cv2.waitKey(0) & 0xff == 27:
    #                 cv2.destroyAllWindows()
                 label = gxlabels[i]+gylabels[j]
                 mydir = myfolder+label
                 myim_name = label+"_"+str(i)+str(j)+"_"+str(l)+"_"+str(k)+".jpg"
                 mydir = mydir+"\\"+myim_name
                 cv2.imwrite(mydir, pic)
                 print "Saving "+mydir


def generateValidation():
    myfolder = os.getcwd();

    myfolder2 = myfolder + "\\Fnt2\\"
    myfolder = myfolder + "\\Fnt6\\"
    if not os.path.exists(myfolder):
        os.makedirs(myfolder)
    for i in x:
        for j in y:
            label = gxlabels[i] + gylabels[j]


    chread = charReader();
    for i in x:
        minusfolder = myfolder2 + gxlabels[i]
        digits_minus, _ = chread.load_digits_by_name(minusfolder)
        digits_minus = random.sample(digits_minus, 2)
        digits_minus = map(file_to_img, digits_minus)
        l = 0;
        for el1 in digits_minus:
            l = l + 1;
            for j in y:
                plusfolder = myfolder2 + gylabels[j]
                digits_plus, _ = chread.load_digits_by_name(plusfolder)
                digits_plus = random.sample(digits_plus, 2)
                digits_plus = map(file_to_img, digits_plus)
                k = 0;
                for el2 in digits_plus:
                    k = k + 1;
                    pic = merge_images(el1, el2)
                    #             cv2.imshow('merge',pic)
                    #             if cv2.waitKey(0) & 0xff == 27:
                    #                 cv2.destroyAllWindows()
                    label = gxlabels[i] + gylabels[j]
                    mydir = myfolder
                    myim_name = label + "_" + str(i) + str(j) + "_" + str(l) + "_" + str(k) + ".jpg"
                    mydir = mydir + "\\" + myim_name
                    cv2.imwrite(mydir, pic)
                    print "Saving " + mydir



generateValidation()