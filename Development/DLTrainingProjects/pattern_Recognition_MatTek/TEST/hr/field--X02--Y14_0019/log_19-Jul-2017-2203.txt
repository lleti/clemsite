
matlab_folder =

    'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\'

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000598 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.649309 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 162 662 .
First test failed for 945 281 .
Second test failed for 945 281 .
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 7J : 0.9528 
 Keeping original estimation  #       point   letter   has_image

  1   [159 657]       7J           1
  2   [548 671]       7K           1
  3   [174 262]       8J           0
  4   [563 275]       8K           0
  5   [935 684]       7L           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="43.5217" FieldYCoordinate="58.8213" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="90" FieldZCoordinateRef="0.0" Map="7J"/>
   <GridPoint FieldXCoordinate="44.1105" FieldYCoordinate="58.8001" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="100" FieldZCoordinateRef="0.0" Map="7K"/>
   <GridPoint FieldXCoordinate="43.5444" FieldYCoordinate="59.4192" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="90" FieldZCoordinateRef="0.0" Map="8J"/>
   <GridPoint FieldXCoordinate="44.1332" FieldYCoordinate="59.3996" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="100" FieldZCoordinateRef="0.0" Map="8K"/>
   <GridPoint FieldXCoordinate="44.6963" FieldYCoordinate="58.7805" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="7L"/>
</GridPointList>
