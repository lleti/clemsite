
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.002329 
Lower Thresh at: 0.400000 
Higher Thresh at: 2.356719 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 5.185000e+02 6.535000e+02 .
First test failed for 133 6.535000e+02 .
Second test failed for 133 6.535000e+02 .
Positive angle: 0
Negative angle: -90
Saving information.
Probability of being the estimated pattern 4K : 0.7572 
 Keeping original estimation  #       point   letter   has_image

  1   [518 659]       4L           1
  2   [139 253]       5K           0
  3   [529 259]       5L           0
  4   [917 271]       5M           0
  5   [908 666]       4M           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="41.0097" FieldYCoordinate="23.1893" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="40" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="4L"/>
   <GridPoint FieldXCoordinate="40.4361" FieldYCoordinate="23.8038" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="100" FieldZCoordinateRef="0.0" Map="5K"/>
   <GridPoint FieldXCoordinate="41.0264" FieldYCoordinate="23.7947" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="5L"/>
   <GridPoint FieldXCoordinate="41.6137" FieldYCoordinate="23.7766" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="5M"/>
   <GridPoint FieldXCoordinate="41.6001" FieldYCoordinate="23.1787" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="40" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="4M"/>
</GridPointList>
