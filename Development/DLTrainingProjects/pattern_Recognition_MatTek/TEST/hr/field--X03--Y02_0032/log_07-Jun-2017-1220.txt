
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000477 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.129274 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 86
Negative angle: -4
Saving information.
Probability of being the estimated pattern 8G : 0.7542 
 Keeping original estimation  #       point   letter   has_image

  1   [343 422]       9G           1
  2   [363 818]       8G           1
  3    [322 28]       aG           0
  4   [732 401]       9H           0
  5   [751 797]       8H           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="38.4121" FieldYCoordinate="24.0737" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="9G"/>
   <GridPoint FieldXCoordinate="38.4423" FieldYCoordinate="23.4743" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="8G"/>
   <GridPoint FieldXCoordinate="38.3803" FieldYCoordinate="24.6701" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="aG"/>
   <GridPoint FieldXCoordinate="39.0009" FieldYCoordinate="24.1055" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="9H"/>
   <GridPoint FieldXCoordinate="39.0296" FieldYCoordinate="23.506" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="8H"/>
</GridPointList>
