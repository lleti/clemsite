
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001321 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.121219 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 7M : 0.4123 
 Keeping original estimation  #       point   letter   has_image

  1   [308 819]       7M           1
  2   [323 423]       8M           1
  3   [712 437]       8N           0
  4   [695 833]       7N           0
  5    [337 30]       9M           0
  6    [728 43]       9N           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="43.4636" FieldYCoordinate="24.2912" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="7M"/>
   <GridPoint FieldXCoordinate="43.4863" FieldYCoordinate="24.8906" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="8M"/>
   <GridPoint FieldXCoordinate="44.0751" FieldYCoordinate="24.8694" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="8N"/>
   <GridPoint FieldXCoordinate="44.0493" FieldYCoordinate="24.27" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="7N"/>
   <GridPoint FieldXCoordinate="43.5074" FieldYCoordinate="25.4855" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="9M"/>
   <GridPoint FieldXCoordinate="44.0993" FieldYCoordinate="25.4658" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="9N"/>
</GridPointList>
