
matlab_folder =

    'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\'

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001140 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.069499 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 387 713 .
First test failed for 1.050000e+01 312 .
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 8E : 0.7987 
 Keeping original estimation  #       point   letter   has_image

  1   [384 713]       8E           1
  2    [13 305]       9D           0
  3   [399 317]       9E           0
  4   [789 332]       9F           0
  5   [773 727]       8F           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="37.6007" FieldYCoordinate="59.3798" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="40" FieldZCoordinateRef="0.0" Map="8E"/>
   <GridPoint FieldXCoordinate="37.0391" FieldYCoordinate="59.9974" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="30" FieldZCoordinateRef="0.0" Map="9D"/>
   <GridPoint FieldXCoordinate="37.6234" FieldYCoordinate="59.9793" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="40" FieldZCoordinateRef="0.0" Map="9E"/>
   <GridPoint FieldXCoordinate="38.2137" FieldYCoordinate="59.9566" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="50" FieldZCoordinateRef="0.0" Map="9F"/>
   <GridPoint FieldXCoordinate="38.1895" FieldYCoordinate="59.3587" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="50" FieldZCoordinateRef="0.0" Map="8F"/>
</GridPointList>
