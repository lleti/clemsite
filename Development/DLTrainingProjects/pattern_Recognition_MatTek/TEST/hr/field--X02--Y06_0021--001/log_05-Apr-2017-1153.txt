
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.005295 
Lower Thresh at: 0.400000 
Higher Thresh at: 1.895247 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 0 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 668 7.835000e+02 .
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 7H : 0.9757 
 Keeping original estimation  #       point   letter   has_image

  1   [271 775]       7H           1
  2   [286 377]       8H           0
  3   [675 389]       8I           0
  4   [663 789]       7I           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="40.1188" FieldYCoordinate="55.3327" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="7H"/>
   <GridPoint FieldXCoordinate="40.1415" FieldYCoordinate="55.9352" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="8H"/>
   <GridPoint FieldXCoordinate="40.7303" FieldYCoordinate="55.917" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="80" FieldZCoordinateRef="0.0" Map="8I"/>
   <GridPoint FieldXCoordinate="40.7122" FieldYCoordinate="55.3115" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="80" FieldZCoordinateRef="0.0" Map="7I"/>
</GridPointList>
