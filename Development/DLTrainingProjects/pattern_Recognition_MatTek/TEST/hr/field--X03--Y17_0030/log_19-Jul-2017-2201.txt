
matlab_folder =

    'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\'

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000664 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.162380 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 4 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 9.805000e+02 952 .
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 6L : 0.8763 
 Keeping original estimation  #        point   letter   has_image

  1    [194 935]       5L           1
  2    [210 537]       6L           1
  3    [584 951]       5M           1
  4    [598 552]       6M           1
  5    [222 143]       7L           0
  6    [611 156]       7M           0
  7    [988 565]       6N           0
  8    [974 960]       5N           0
  9   [1002 172]       7N           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="44.6701" FieldYCoordinate="57.5919" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="5L"/>
   <GridPoint FieldXCoordinate="44.6944" FieldYCoordinate="58.1943" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="6L"/>
   <GridPoint FieldXCoordinate="45.2605" FieldYCoordinate="57.5677" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="5M"/>
   <GridPoint FieldXCoordinate="45.2817" FieldYCoordinate="58.1716" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="6M"/>
   <GridPoint FieldXCoordinate="44.7125" FieldYCoordinate="58.7907" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="7L"/>
   <GridPoint FieldXCoordinate="45.3013" FieldYCoordinate="58.771" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="7M"/>
   <GridPoint FieldXCoordinate="45.872" FieldYCoordinate="58.152" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="6N"/>
   <GridPoint FieldXCoordinate="45.8508" FieldYCoordinate="57.5541" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="5N"/>
   <GridPoint FieldXCoordinate="45.8932" FieldYCoordinate="58.7468" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="7N"/>
</GridPointList>
