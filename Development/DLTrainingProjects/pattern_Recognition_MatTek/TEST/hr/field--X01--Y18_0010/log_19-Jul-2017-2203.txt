
matlab_folder =

    'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\'

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001160 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.882830 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 7L : 0.9500 
 Keeping original estimation  #       point   letter   has_image

  1   [399 853]       7L           1
  2   [415 457]       8L           1
  3   [805 470]       8M           0
  4   [789 865]       7M           0
  5    [428 62]       9L           0
  6    [819 76]       9M           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="44.6986" FieldYCoordinate="58.7848" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="7L"/>
   <GridPoint FieldXCoordinate="44.7228" FieldYCoordinate="59.3842" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="8L"/>
   <GridPoint FieldXCoordinate="45.3132" FieldYCoordinate="59.3645" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="8M"/>
   <GridPoint FieldXCoordinate="45.2889" FieldYCoordinate="58.7666" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="7M"/>
   <GridPoint FieldXCoordinate="44.7425" FieldYCoordinate="59.9821" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="9L"/>
   <GridPoint FieldXCoordinate="45.3344" FieldYCoordinate="59.9609" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="9M"/>
</GridPointList>
