
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000981 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.925196 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 4 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 86
Negative angle: -4
Saving information.
Probability of being the estimated pattern aN : 0.9675 
 Keeping original estimation  #       point   letter   has_image

  1   [169 601]       aN           1
  2   [189 994]       9N           1
  3   [560 580]       aO           1
  4   [578 972]       9O           1
  5   [149 207]       bN           0
  6   [538 184]       bO           0
  7   [928 167]       bP           0
  8   [946 559]       aP           0
  9   [965 951]       9P           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="42.5264" FieldYCoordinate="24.9173" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="aN"/>
   <GridPoint FieldXCoordinate="42.5567" FieldYCoordinate="24.3225" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="9N"/>
   <GridPoint FieldXCoordinate="43.1183" FieldYCoordinate="24.9491" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="aO"/>
   <GridPoint FieldXCoordinate="43.1455" FieldYCoordinate="24.3558" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="9O"/>
   <GridPoint FieldXCoordinate="42.4962" FieldYCoordinate="25.5137" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="110" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="bN"/>
   <GridPoint FieldXCoordinate="43.085" FieldYCoordinate="25.5485" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="110" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="bO"/>
   <GridPoint FieldXCoordinate="43.6753" FieldYCoordinate="25.5743" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="110" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="bP"/>
   <GridPoint FieldXCoordinate="43.7026" FieldYCoordinate="24.9809" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="aP"/>
   <GridPoint FieldXCoordinate="43.7313" FieldYCoordinate="24.3875" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="9P"/>
</GridPointList>
