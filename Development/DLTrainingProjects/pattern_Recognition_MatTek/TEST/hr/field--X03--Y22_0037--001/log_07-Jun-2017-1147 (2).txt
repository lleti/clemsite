
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001012 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.485394 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 4 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 86
Negative angle: -4
Saving information.
Probability of being the estimated pattern 9P : 0.6623 
 Keeping original estimation  #       point   letter   has_image

  1   [133 543]       9P           1
  2   [153 943]       8P           1
  3   [522 525]       9Q           1
  4   [541 923]       8Q           1
  5   [113 153]       aP           0
  6   [502 130]       aQ           0
  7   [893 112]       aR           0
  8   [908 505]       9R           0
  9   [928 901]       8R           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="43.7489" FieldYCoordinate="24.3944" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="9P"/>
   <GridPoint FieldXCoordinate="43.7791" FieldYCoordinate="23.7889" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="8P"/>
   <GridPoint FieldXCoordinate="44.3377" FieldYCoordinate="24.4216" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="160" FieldZCoordinateRef="0.0" Map="9Q"/>
   <GridPoint FieldXCoordinate="44.3664" FieldYCoordinate="23.8192" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="160" FieldZCoordinateRef="0.0" Map="8Q"/>
   <GridPoint FieldXCoordinate="43.7186" FieldYCoordinate="24.9847" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="aP"/>
   <GridPoint FieldXCoordinate="44.3074" FieldYCoordinate="25.0195" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="160" FieldZCoordinateRef="0.0" Map="aQ"/>
   <GridPoint FieldXCoordinate="44.8992" FieldYCoordinate="25.0468" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="170" FieldZCoordinateRef="0.0" Map="aR"/>
   <GridPoint FieldXCoordinate="44.922" FieldYCoordinate="24.4519" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="170" FieldZCoordinateRef="0.0" Map="9R"/>
   <GridPoint FieldXCoordinate="44.9522" FieldYCoordinate="23.8525" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="170" FieldZCoordinateRef="0.0" Map="8R"/>
</GridPointList>
