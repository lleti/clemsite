
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001390 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.521059 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
8 peaks found.

ans =

Subscript indices must either be real positive integers or logicals.

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('alternativePeaks>searchNewPeaksForAngle', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\alternativePeaks.m', 23)" style="font-weight:bold">alternativePeaks>searchNewPeaksForAngle</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\alternativePeaks.m',23,0)">line 23</a>)
        signal_pos = R(:,90+angle);

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('alternativePeaks', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\alternativePeaks.m', 12)" style="font-weight:bold">alternativePeaks</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\alternativePeaks.m',12,0)">line 12</a>)
          newpeaks = searchNewPeaksForAngle(R,ipeaks,angles(i));

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('getPeaks', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\getPeaks.m', 19)" style="font-weight:bold">getPeaks</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\getPeaks.m',19,0)">line 19</a>)
npeaks = alternativePeaks(R,ipeaks); % Refine, again, based on the grid definition

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('findBestPeaks', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\findBestPeaks.m', 60)" style="font-weight:bold">findBestPeaks</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\findBestPeaks.m',60,0)">line 60</a>)
    [npeaks,error] = getPeaks(Rhigh,gridsize); 

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('glod', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\glod.m', 30)" style="font-weight:bold">glod</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\glod.m',30,0)">line 30</a>)
    [npeaks,R,error] = findBestPeaks(FSWT,ORIENTIM,softParams.K,1,gridsize,softParams.dispim,p_angles);

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('runLD_LM', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\runLD_LM.m', 79)" style="font-weight:bold">runLD_LM</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\runLD_LM.m',79,0)">line 79</a>)
        [images,fpoints,info,error] = glod(img,softParams,folder,tag,p_angles,false);

