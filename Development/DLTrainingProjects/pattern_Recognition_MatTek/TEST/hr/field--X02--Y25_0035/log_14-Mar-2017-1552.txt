
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001467 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.536185 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 0 peaks found during refinement.
Finding Lines by Projected Orientations completed.

ans =

Subscript indices must either be real positive integers or logicals.

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('tlines', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\tlines.m', 35)" style="font-weight:bold">tlines</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\tlines.m',35,0)">line 35</a>)
     if(tmpimg(ipeaks(k,1),j)>0) 

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('calibrateIntersections>adjustLines', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\calibrateIntersections.m', 221)" style="font-weight:bold">calibrateIntersections>adjustLines</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\calibrateIntersections.m',221,0)">line 221</a>)
        rlines = tlines(ic,mp); 

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('calibrateIntersections', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\calibrateIntersections.m', 36)" style="font-weight:bold">calibrateIntersections</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\helpers\calibrateIntersections.m',36,0)">line 36</a>)
        m_ipoints= adjustLines(w,wd,avx,avy,iim,bwedge,angles,folder); 

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('glod', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\glod.m', 54)" style="font-weight:bold">glod</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\glod.m',54,0)">line 54</a>)
    [mpoints] = calibrateIntersections(input_im,BWEDGE,goodlines,npeaks,folder);

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('runLD_LM', 'C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\runLD_LM.m', 79)" style="font-weight:bold">runLD_LM</a> (<a href="matlab: opentoline('C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\runLD_LM.m',79,0)">line 79</a>)
        [images,fpoints,info,error] = glod(img,softParams,folder,tag,p_angles,false);

