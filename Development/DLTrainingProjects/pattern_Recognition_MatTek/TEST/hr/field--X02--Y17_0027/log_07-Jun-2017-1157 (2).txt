
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000921 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.882192 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 86
Negative angle: -4
Saving information.
Probability of being the estimated pattern 6O : 0.8608 
 Keeping original estimation  #       point   letter   has_image

  1   [131 726]       6N           1
  2   [520 707]       6O           1
  3   [112 333]       7N           0
  4   [501 311]       7O           0
  5   [890 292]       7P           0
  6   [907 685]       6P           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="42.6664" FieldYCoordinate="22.5322" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="6N"/>
   <GridPoint FieldXCoordinate="43.2552" FieldYCoordinate="22.5609" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="6O"/>
   <GridPoint FieldXCoordinate="42.6376" FieldYCoordinate="23.1271" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="7N"/>
   <GridPoint FieldXCoordinate="43.2264" FieldYCoordinate="23.1604" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="7O"/>
   <GridPoint FieldXCoordinate="43.8152" FieldYCoordinate="23.1891" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="7P"/>
   <GridPoint FieldXCoordinate="43.841" FieldYCoordinate="22.5942" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="6P"/>
</GridPointList>
