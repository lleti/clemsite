
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000542 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.997744 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 4 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 9.992500e+02 1.003500e+03 .
Second test failed for 9.992500e+02 1.003500e+03 .
Positive angle: 2
Negative angle: -88
Saving information.
Probability of being the estimated pattern 6O : 0.9162 
 Keeping original estimation  #       point   letter   has_image

  1   [214 976]       5O           1
  2   [239 577]       6O           1
  3   [603 996]       5P           1
  4   [625 600]       6P           1
  5   [263 181]       7O           0
  6   [649 202]       7P           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="43.0009" FieldYCoordinate="23.4059" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="5O"/>
   <GridPoint FieldXCoordinate="43.0388" FieldYCoordinate="24.0098" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="6O"/>
   <GridPoint FieldXCoordinate="43.5898" FieldYCoordinate="23.3756" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="5P"/>
   <GridPoint FieldXCoordinate="43.6231" FieldYCoordinate="23.975" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="6P"/>
   <GridPoint FieldXCoordinate="43.0751" FieldYCoordinate="24.6092" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="7O"/>
   <GridPoint FieldXCoordinate="43.6594" FieldYCoordinate="24.5774" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="7P"/>
</GridPointList>
