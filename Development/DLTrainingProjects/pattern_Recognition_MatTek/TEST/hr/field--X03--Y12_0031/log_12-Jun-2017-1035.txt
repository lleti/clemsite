
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001431 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.122596 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 4.725000e+02 6.385000e+02 .
First test failed for 84 632 .
First test failed for 4.795000e+02 242 .
Second test failed for 4.795000e+02 242 .
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 3K : 0.7152 
 Keeping original estimation  #       point   letter   has_image

  1    [82 622]       3J           1
  2   [471 640]       3K           1
  3    [97 226]       4J           0
  4   [876 257]       4L           0
  5   [860 654]       3L           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="40.4858" FieldYCoordinate="56.3098" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="30" FieldYCoordinateRef="90" FieldZCoordinateRef="0.0" Map="3J"/>
   <GridPoint FieldXCoordinate="41.0746" FieldYCoordinate="56.2825" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="30" FieldYCoordinateRef="100" FieldZCoordinateRef="0.0" Map="3K"/>
   <GridPoint FieldXCoordinate="40.5085" FieldYCoordinate="56.9092" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="40" FieldYCoordinateRef="90" FieldZCoordinateRef="0.0" Map="4J"/>
   <GridPoint FieldXCoordinate="41.6876" FieldYCoordinate="56.8623" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="40" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="4L"/>
   <GridPoint FieldXCoordinate="41.6634" FieldYCoordinate="56.2613" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="30" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="3L"/>
</GridPointList>
