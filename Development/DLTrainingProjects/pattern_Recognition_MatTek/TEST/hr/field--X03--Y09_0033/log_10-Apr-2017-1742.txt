
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.001769 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.032852 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 4 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 9.725000e+02 1.965000e+02 .
Positive angle: 1
Negative angle: -89
Saving information.
Probability of being the estimated pattern 6G : 0.9530 
 Keeping original estimation  #       point   letter   has_image

  1   [172 970]       5G           1
  2   [187 574]       6G           1
  3   [562 986]       5H           1
  4   [576 586]       6H           1
  5   [201 180]       7G           0
  6   [590 191]       7H           0
  7   [965 599]       6I           0
  8   [952 996]       5I           0
  9   [980 207]       7I           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="39.8652" FieldYCoordinate="23.1927" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="5G"/>
   <GridPoint FieldXCoordinate="39.8879" FieldYCoordinate="23.7921" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="6G"/>
   <GridPoint FieldXCoordinate="40.4555" FieldYCoordinate="23.1685" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="5H"/>
   <GridPoint FieldXCoordinate="40.4767" FieldYCoordinate="23.774" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="6H"/>
   <GridPoint FieldXCoordinate="39.9091" FieldYCoordinate="24.3885" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="7G"/>
   <GridPoint FieldXCoordinate="40.4979" FieldYCoordinate="24.3719" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="7H"/>
   <GridPoint FieldXCoordinate="41.0655" FieldYCoordinate="23.7543" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="80" FieldZCoordinateRef="0.0" Map="6I"/>
   <GridPoint FieldXCoordinate="41.0458" FieldYCoordinate="23.1534" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="80" FieldZCoordinateRef="0.0" Map="5I"/>
   <GridPoint FieldXCoordinate="41.0882" FieldYCoordinate="24.3476" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="80" FieldZCoordinateRef="0.0" Map="7I"/>
</GridPointList>
