
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000994 
Lower Thresh at: 0.400000 
Higher Thresh at: 3.417434 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 4 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 86
Negative angle: -4
Saving information.
Probability of being the estimated pattern 8M : 0.6749 
 Keeping original estimation  #       point   letter   has_image

  1    [87 456]       9L           1
  2   [105 852]       8L           1
  3   [473 436]       9M           1
  4   [490 832]       8M           1
  5     [65 65]       aL           0
  6    [451 42]       aM           0
  7    [834 33]       aN           0
  8   [861 416]       9N           0
  9   [881 814]       8N           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="41.3839" FieldYCoordinate="24.2528" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="9L"/>
   <GridPoint FieldXCoordinate="41.4111" FieldYCoordinate="23.6534" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="8L"/>
   <GridPoint FieldXCoordinate="41.9682" FieldYCoordinate="24.2831" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="9M"/>
   <GridPoint FieldXCoordinate="41.9939" FieldYCoordinate="23.6837" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="8M"/>
   <GridPoint FieldXCoordinate="41.3506" FieldYCoordinate="24.8447" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="110" FieldZCoordinateRef="0.0" Map="aL"/>
   <GridPoint FieldXCoordinate="41.9349" FieldYCoordinate="24.8795" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="120" FieldZCoordinateRef="0.0" Map="aM"/>
   <GridPoint FieldXCoordinate="42.5146" FieldYCoordinate="24.8931" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="aN"/>
   <GridPoint FieldXCoordinate="42.5555" FieldYCoordinate="24.3134" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="9N"/>
   <GridPoint FieldXCoordinate="42.5858" FieldYCoordinate="23.7109" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="8N"/>
</GridPointList>
