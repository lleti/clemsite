
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000939 
Lower Thresh at: 0.400000 
Higher Thresh at: 4.542537 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 0 peaks found during refinement.
Finding Lines by Projected Orientations completed.
Positive angle: 2
Negative angle: -88
Saving information.
Probability of being the estimated pattern 7H : 0.8666 
 Keeping original estimation  #       point   letter   has_image

  1   [298 719]       7H           1
  2   [321 326]       8H           0
  3   [710 348]       8I           0
  4   [686 743]       7I           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="38.9094" FieldYCoordinate="24.8121" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="7H"/>
   <GridPoint FieldXCoordinate="38.9443" FieldYCoordinate="25.4069" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="70" FieldZCoordinateRef="0.0" Map="8H"/>
   <GridPoint FieldXCoordinate="39.5331" FieldYCoordinate="25.3736" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="80" FieldYCoordinateRef="80" FieldZCoordinateRef="0.0" Map="8I"/>
   <GridPoint FieldXCoordinate="39.4967" FieldYCoordinate="24.7757" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="70" FieldYCoordinateRef="80" FieldZCoordinateRef="0.0" Map="7I"/>
</GridPointList>
