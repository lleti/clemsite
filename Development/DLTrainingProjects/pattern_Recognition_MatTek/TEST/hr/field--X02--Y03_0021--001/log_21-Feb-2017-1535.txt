
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.002395 
Lower Thresh at: 0.400000 
Higher Thresh at: 2.307921 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
8 peaks found.
A total of 2 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 424 7.545000e+02 .
Second test failed for 424 7.545000e+02 .
Positive angle: 0
Negative angle: -90
Saving information.
Probability of being the estimated pattern 9F : 0.5073 
 Keeping original estimation  #       point   letter   has_image

  1    [33 746]       9E           1
  2    [42 347]       aE           0
  3   [426 356]       aF           0
  4   [818 367]       aG           0
  5   [811 764]       9G           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="36.9008" FieldYCoordinate="26.2503" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="40" FieldZCoordinateRef="0.0" Map="9E"/>
   <GridPoint FieldXCoordinate="36.9145" FieldYCoordinate="26.8543" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="40" FieldZCoordinateRef="0.0" Map="aE"/>
   <GridPoint FieldXCoordinate="37.4957" FieldYCoordinate="26.8406" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="50" FieldZCoordinateRef="0.0" Map="aF"/>
   <GridPoint FieldXCoordinate="38.0891" FieldYCoordinate="26.824" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="100" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="aG"/>
   <GridPoint FieldXCoordinate="38.0785" FieldYCoordinate="26.2231" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="90" FieldYCoordinateRef="60" FieldZCoordinateRef="0.0" Map="9G"/>
</GridPointList>
