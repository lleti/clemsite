
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.002355 
Lower Thresh at: 0.400000 
Higher Thresh at: 2.254664 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 4 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 489 112 .
Positive angle: 2
Negative angle: -88
Saving information.
Probability of being the estimated pattern 3O : 0.9003 
 Keeping original estimation  #       point   letter   has_image

  1    [89 884]       3N           1
  2    [88 488]       4N           1
  3   [457 901]       3O           1
  4   [477 507]       4O           1
  5    [105 96]       5N           0
  6   [493 111]       5O           0
  7   [865 522]       4P           0
  8   [847 918]       3P           0
  9   [881 129]       5P           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="43.0066" FieldYCoordinate="51.0053" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="30" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="3N"/>
   <GridPoint FieldXCoordinate="43.0051" FieldYCoordinate="51.6048" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="40" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="4N"/>
   <GridPoint FieldXCoordinate="43.5637" FieldYCoordinate="50.9796" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="30" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="3O"/>
   <GridPoint FieldXCoordinate="43.5939" FieldYCoordinate="51.576" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="40" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="4O"/>
   <GridPoint FieldXCoordinate="43.0308" FieldYCoordinate="52.1981" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="130" FieldZCoordinateRef="0.0" Map="5N"/>
   <GridPoint FieldXCoordinate="43.6182" FieldYCoordinate="52.1754" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="140" FieldZCoordinateRef="0.0" Map="5O"/>
   <GridPoint FieldXCoordinate="44.1812" FieldYCoordinate="51.5533" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="40" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="4P"/>
   <GridPoint FieldXCoordinate="44.154" FieldYCoordinate="50.9539" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="30" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="3P"/>
   <GridPoint FieldXCoordinate="44.2055" FieldYCoordinate="52.1482" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="150" FieldZCoordinateRef="0.0" Map="5P"/>
</GridPointList>
