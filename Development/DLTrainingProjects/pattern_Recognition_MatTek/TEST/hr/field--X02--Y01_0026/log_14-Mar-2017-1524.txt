
matlab_folder =

C:\Users\Schwab\Documents\msite\matlab_msite\GLOD\

-  CLAHE:  TRUE 
Preprocessing done: 
-  Gaussian Filter : 1.200000 
Automatic Canny done 
Percentage of pixels threshold reached at: 0.000789 
Lower Thresh at: 0.400000 
Higher Thresh at: 5.369474 
Wiener filter done 
Orientations done 
Starting SWT with strokeWidth of 20. 
SWT done 
Removing ill components 
Removing done 
Finding peaks by non maximum supression 
Angle sum :90 
8 peaks found.
A total of 0 peaks found during refinement.
Finding Lines by Projected Orientations completed.
First test failed for 673 3.365000e+02 .
Positive angle: 2
Negative angle: -88
Saving information.
Probability of being the estimated pattern 5F : 0.0486 
 Evaluating patterns with best probabilities. Selected final pattern: 5E : 0.8669 
   #       point   letter   has_image

  1   [266 715]       5E           1
  2   [289 319]       6E           0
  3   [679 342]       6F           0
  4   [653 737]       5F           0

<?xml version="1.0" encoding="utf-8"?>
<GridPointList version="1.0">
   <GridPoint FieldXCoordinate="37.669" FieldYCoordinate="23.6725" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="40" FieldZCoordinateRef="0.0" Map="5E"/>
   <GridPoint FieldXCoordinate="37.7038" FieldYCoordinate="24.2719" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="40" FieldZCoordinateRef="0.0" Map="6E"/>
   <GridPoint FieldXCoordinate="38.2941" FieldYCoordinate="24.2371" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="60" FieldYCoordinateRef="50" FieldZCoordinateRef="0.0" Map="6F"/>
   <GridPoint FieldXCoordinate="38.2548" FieldYCoordinate="23.6392" FieldZCoordinate="0.0"/>
   <GridPointRef FieldXCoordinateRef="50" FieldYCoordinateRef="50" FieldZCoordinateRef="0.0" Map="5F"/>
</GridPointList>
