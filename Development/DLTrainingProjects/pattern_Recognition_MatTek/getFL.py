

import os, shutil
from os import listdir
from os.path import isfile, join
import glob, re

def filterPick( myList, myString):
    pattern = re.compile(myString);
    indices = [i for i, x in enumerate(myList) if pattern.search(x)]
    return [myList[i] for i in indices]



def load_RL(mypath):
    RLpath ='.\\TEST\\RL'
    rl_name = RLpath+'\\rl_'
    counter = 0
    for di in listdir(mypath):
        adir = mypath + '\\' + di;
        onlyfiles = [f for f in listdir(adir) if isfile(join(adir, f))]
        # Extract RL
        RLfiles = filterPick(onlyfiles,'.*RL.*')
        for el in RLfiles:
            rl_name_in = rl_name+str(counter)+".tif"
            file_n = adir + '\\' + el
            shutil.copy(file_n,rl_name_in)
            counter=counter+1
            print(rl_name_in)
    print('END')
    return

def load_LETTERS(mypath):
    letters_path = '.\\TEST\\LETTERS'

    counter = 0
    for di in listdir(mypath):
        adir = mypath + '\\' + di;
        onlydirs = [f for f in listdir(adir) if not isfile(join(adir, f))]
        # Extract RL
        let_dir = filterPick(onlydirs,'ld.*')
        for el in let_dir:
            ndir = adir+"\\"+el
            onlydirs2 = [f for f in listdir(ndir) if not isfile(join(ndir, f))]
            ldirs = filterPick(onlydirs2, 'letters.*')
            if(ldirs):
                onlyfiles = [f for f in listdir(ndir+"\\"+ldirs[0]) if isfile(join(ndir+"\\"+ldirs[0], f))]
                for lfile in onlyfiles:
                    l_name_in = letters_path+'\\'+lfile[:2]+"_"+str(counter)+".tif"
                    file_n = ndir+"\\"+ldirs[0]+'\\'+lfile
                    shutil.copy(file_n,l_name_in)
                    counter=counter+1
                    print(l_name_in)
    print('END')
    return


def to_dir(mypath,where_path):
    ofiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for el in ofiles:
        code = el[:2]
        if not os.path.exists(where_path+"\\"+code):
            os.mkdir(where_path+"\\"+code)
        shutil.copy(mypath+"\\"+el,where_path+"\\"+code)
    print("END")
#load_RL(".\\TEST\\hr")
# load_LETTERS(".\\TEST\\hr")
to_dir(".\\TEST\\LETTERS",".\\Fnt2")