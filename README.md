#This is a preliminary version of the software for CLEMSite software
---------------------------------------------------------------------
Author : Jose Miguel Serra Lleti
email : lleti@embl.de
----------------------------------------------------------------------------------------------------------
v 0.1.17 Phoenix Known issues to be fixed 

For Clients and Server:
1.- 

For Msite2 (Navigator) :
    1.- When a point is updated, should go by an event that updates the row and not the full table, except cases like deletions or insertions.
    2.- Icon of application is not showing
    3.- When the All button is pushed, the multithreading is too slow if files have to be read. The solution is to differentiate between a fresh a start (then execute parallel) or when read from disk (serial).
	4.- Testing parameters should be easier for the line detection. We have to add the possibility of changing a few of the parameters in the GUI.
	5.- Also, the next image in the Z stack has to be tested if the first fails.

 For CLEMSiteServer
    1.- Folders are not remembered in CLEMSiteServer when opened again
    2.- Change configuration has to show a default text editor.

For Msite4 (Multisite:
    1.- Msite4 crashing if CP has been done and we start another cell. The job in the server doesn't renew.
    2.- If STOP, it should be possible to start again. Be sure that all variables are reseted and it is safe to restart without closing the application if something happens.


	

**IMPROVEMENTS IN FUTURE VERSIONS OF THE SOFTWARE:**
		
		1) Better focus points detector ML based.
		2) Better detection of focus losing by incorporating frequency changes in regions.
		3) Store Z height. If stored, CP is not executed again. When automatic CP is finished, in the message back it sends the Z height for this particular cell.
        The value is stored in the list of POIs and next time, when this cell is executed, if the Z height is detected, it will jump the CP step and instead it will move directly to the next position with the Z included in Phase 1. 
		4) Add a timer for the scan in addition to the progress bar that can be seen in the GUI. 
		5) Project Folder from CLEMSiteServer given by user is not stored in user preferences from CLEMSite. Ideally, just one source of folders should be given, not 2.

		
		
		PROBLEM Solving during installation

The required paths must be there:


For Windows 10/8/7:
Open System Properties (Right click Computer in the start menu, or use the keyboard shortcut Win+Pause)
Click Advanced system settings in the sidebar.
Click Environment Variables...
Select PATH in the System variables section
Click Edit
Add Python's path to the end of the list (the paths are separated by semicolons). For example:

(path:\to\Anaconda3\;path:\to\Anaconda3\Scripts;path:\to\Anaconda3\Library\mingw-w64\bin;path:\to\Anaconda3\Library\usr\bin;path:\to\Anaconda3\Library\bin)