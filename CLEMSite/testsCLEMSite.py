import unittest
import json
import os
import sys
import cv2
from common.image_an.lineDetector.gridLineDetector import GridLineDetector as GLD
from common.image_an.readers import getInfoTiffOME,getInfoHeader
import glob
# def test_startGLD():
#     myGLD = GLD()
#     m_folder = 'C:\\Users\\JMS\\Documents\\CLEMSite\\OLD_python'
#     input_image_path = m_folder + '\\tests_GLD\\test1\\grid_0032_1--LM--RL - Reflected Light--10x--z1.tif'
#     output_folder = '.'
#     parameters_file = m_folder + '\\tests_GLD\\user_test1.pref'
#     image_metadata = m_folder + '\\tests_GLD\\test1\\field--X03--Y06_0032_info.txt'
#     myGLD.runGLD(input_image_path, output_folder, parameters_file, image_metadata, 'LM', True)
#     # self.assertTrue(True)
# def test_LM(self):
#         myGLD = GLD()
#         m_folder = 'E:\\testNMTL\\RL'
#         input_image_path = m_folder + '\\test6.tif'
#         output_folder = 'E:\\testNMTL\\RL'
#         parameters_file = '.\\preferences\\user.pref'
#         image_metadata = m_folder + '\\test1_info.txt'
#         myGLD.runGLD(input_image_path, output_folder, parameters_file, image_metadata, 'LM', True)
#         self.assertTrue(True)

class GLDTestCase(unittest.TestCase):
    def test_SEM(self):
        myGLD = GLD()
        m_folder = 'E:\\testNMTL\\FullSQUARE'
        files = glob.glob(m_folder+"\*.tif")
        output_folder = 'E:\\testNMTL\\FullSQUARE'
        parameters_file = '.\\preferences\\user.pref'
        image_metadata = 'E:\\testNMTL\\SEM\\testSEM.txt'

        for file in files:
            input_image_path = file #'E:\\testNMTL\\FullSQUARE\\6R.tif'
            myGLD.runGLD(input_image_path, output_folder, parameters_file, image_metadata, 'SEM', True)
        self.assertTrue(True)




if __name__ == '__main__':
    unittest.main()