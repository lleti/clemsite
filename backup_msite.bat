@echo on
REM copy documents
SETLOCAL
SET "sourcedir=C:\Users\Schwab\Documents\GitHub\MSite-master"
SET  TIMESTAMP=%DATE%-%TIME:~0,2%-%TIME:~3,2%-%TIME:~6,2%
SET "destdir=D:\backups_msite\msite_nomat_%TIMESTAMP%"

MD "%destdir%"


XCOPY "%sourcedir%\*.*" "%destdir%\" /c /s /r /d /y /i 

